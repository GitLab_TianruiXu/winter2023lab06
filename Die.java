import java.util.Random;
public class Die {
	private int faceValue;
	private Random r;
	
	public Die() {
		this.faceValue = 1;
		this.r = new Random();
	}
	
	public int getFaceValue() {
		return this.faceValue;
	}
	
	public void roll() {
		this.faceValue = r.nextInt(6) + 1;
	}
	
	public String toString() {
		return this.faceValue + "";
	}
}
