
public class Jackpot {

	public static void main(String[] args) {
		System.out.println("Welcome to Jackpot game");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		while(!gameOver) {
			System.out.println(board);
			if(board.playATurn()) {
				gameOver = true;
			}else {
				numOfTilesClosed ++;
			}
		}
		if(numOfTilesClosed >= 7) {
			System.out.println("You have won by reaching the jackpot");
		}else {
			System.out.println("You have lost, you did not reach the jackpot");
		}
	}

}
