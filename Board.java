
public class Board {
	private Die d1;
	private Die d2;
	private boolean[] tiles;
	
	public Board() {
		this.d1 = new Die();
		this.d2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public String toString() {
		String tileList = "";
		for(int i = 0; i < this.tiles.length; i++) {
			if(this.tiles[i]) {
				tileList += "x";
			}else{
				tileList += i + 1;
			}
			tileList += " ";
		}
		return tileList;
	}
	
	public boolean playATurn() {
		this.d1.roll();
		this.d2.roll();
		System.out.println("First die: " + this.d1);
		System.out.println("Second die: " + this.d2);
		int sumOfDice = this.d1.getFaceValue() + this.d2.getFaceValue();
		if(!this.tiles[sumOfDice - 1]) {
			this.tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}else if(!this.tiles[this.d1.getFaceValue() - 1]) {
			this.tiles[this.d1.getFaceValue() - 1] = true;
			System.out.println("Closing tile with the same value as die one");
			return false;
		}else if(!this.tiles[this.d2.getFaceValue() - 1]) {
			this.tiles[this.d2.getFaceValue() - 1] = true;
			System.out.println("Closing tile with the same value as die two");
			return false;
		}else {
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
}
